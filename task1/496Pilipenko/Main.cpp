#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>


class MobiusStripApplication : public Application
{
public:
	MobiusStripApplication(int _count) : count(_count) {}
	MeshPtr _mobiusStrip;
	int count;

	ShaderProgramPtr _shader;

	void makeScene() override
	{
		Application::makeScene();

		_cameraMover = std::make_shared<FreeCameraMover>();

		//������� MobiusStrip
		_mobiusStrip = makeMobiusStrip(count);
		_mobiusStrip->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

		//������� ��������� ���������        
		_shader = std::make_shared<ShaderProgram>("496PilipenkoData/shaderNormal.vert", "496PilipenkoData/shader.frag");
	}

	void update() override
	{
		Application::update();
	}

	void draw() override
	{
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//������������� ������
		_shader->use();

		//������������� ����� �������-����������
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		//������ ��� ������
		_shader->setMat4Uniform("modelMatrix", _mobiusStrip->modelMatrix());
		_mobiusStrip->draw();
	}

	void handleKey(int key, int scancode, int action, int mods) override
	{
		Application::handleKey(key, scancode, action, mods);

		const double multiplier = 0.5;

		if (action == GLFW_PRESS) {
			if (key == GLFW_KEY_MINUS) {
				count -= 2;
				_mobiusStrip = makeMobiusStrip(count);
				update();
			}
			if (key == GLFW_KEY_EQUAL) {
				count += 2;
				_mobiusStrip = makeMobiusStrip(count);
				update();
			}
		}
	}
};

int main()
{
	MobiusStripApplication app(500);
	app.start();

	return 0;
}