#include <Mesh.hpp>

#include <iostream>
#include <vector>

MeshPtr makeMobiusStrip(unsigned int N)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    float pi = (float)glm::pi<float>();

    for (unsigned int i = 0; i < N; i++)
    {
        float u = -0.4f + 0.4f / N * i;
        float u1 = -0.4f + 0.4f / N * (i + 1);

        for (unsigned int j = 0; j < N; j++)
        {
            float v =  2.0f * pi / N * j;
            float v1 = 2.0f * pi / N * (j + 1);

            //Первый треугольник, образующий квад
            for (int k = 0; k < 3; k += 2) {
                vertices.push_back(glm::vec3(X(u, v + k * pi), Y(u, v + k * pi), Z(u, v + k * pi)));
                vertices.push_back(glm::vec3(X(u1, v1 + k * pi), Y(u1, v1 + k * pi), Z(u1, v1 + k * pi)));
                vertices.push_back(glm::vec3(X(u1, v + k * pi), Y(u1, v + k * pi), Z(u1, v + k * pi)));
                
                normals.push_back(glm::normalize(glm::vec3(Nx(u, v), Ny(u, v), Nz(u, v))));
                normals.push_back(glm::normalize(glm::vec3(Nx(u1, v1), Ny(u1, v1), Nz(u1, v1))));
                normals.push_back(glm::normalize(glm::vec3(Nx(u1, v), Ny(u1, v), Nz(u1, v))));
            
                texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / N));
                texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / N));
                texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)i / N));
            }
            
            //Второй треугольник, образующий квад
            for (int k = 0; k < 3; k += 2) {
                vertices.push_back(glm::vec3(X(u, v + k * pi), Y(u, v + k * pi), Z(u, v + k * pi)));
                vertices.push_back(glm::vec3(X(u, v1 + k * pi), Y(u, v1 + k * pi), Z(u, v1 + k * pi)));
                vertices.push_back(glm::vec3(X(u1, v1 + k * pi), Y(u1, v1 + k * pi), Z(u1, v1 + k * pi)));

                normals.push_back(glm::normalize(glm::vec3(Nx(u, v), Ny(u, v), Nz(u, v))));
                normals.push_back(glm::normalize(glm::vec3(Nx(u, v1), Ny(u, v1), Nz(u, v1))));
                normals.push_back(glm::normalize(glm::vec3(Nx(u1, v1), Ny(u1, v1), Nz(u1, v1))));  

                texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / N));
                texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)(i + 1) / N));
                texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / N));  
            }

        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);         
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Mobius srip is created with " << vertices.size() << " vertices\n";

    return mesh;
}

float X( float u, float v )
{
	return cos(v) * (1 + u * cos(v / 2));
}

float Y( float u, float v )
{
	return sin(v) * (1 + u * cos(v / 2));
}

float Z( float u, float v )
{
	return u * sin(v / 2);
}

float Nx( float u, float v )
{
    return 2 * sin(v / 2) * (cos(v) - u * sin(v / 2) * sin(v));
}

float Ny( float u, float v )
{
    return 2 * sin(v / 2) * sin(v) + u * (cos(v) + sin(v) * sin(v));
}

float Nz( float u, float v )
{
    return - 2 * cos(v / 2) * (1 + u * cos(v / 2));
}