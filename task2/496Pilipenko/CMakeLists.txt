set(SRC_FILES
    Main.cpp
    common/Application.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
    common/Framebuffer.cpp
    common/Texture.cpp
)

set(HEADER_FILES
    common/Application.hpp
    common/Camera.cpp
    common/Mesh.hpp
    common/ShaderProgram.hpp
    common/Framebuffer.hpp
    common/Texture.hpp
    common/LightInfo.hpp
)

set(ALL_FILES
        ${SRC_FILES}
        ${HEADER_FILES}
        )

set(SHADER_FILES
    496PilipenkoData/shader.frag
    496PilipenkoData/shaderNormal.vert
    496PilipenkoData/marker.vert
    496PilipenkoData/marker.frag
    496PilipenkoData/texture.vert
    496PilipenkoData/texture.frag
    496PilipenkoData/water.jpg
    496PilipenkoData/wood.png
)

source_group("Shaders" FILES
        ${SHADER_FILES}
)

include_directories(common)

MAKE_TASK(496Pilipenko 2 "${ALL_FILES}")
