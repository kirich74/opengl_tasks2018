#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include <LightInfo.hpp>
#include <Texture.hpp>

#include <iostream>
#include <vector>

std::string data = "496PilipenkoData/";
/**
MobiusStrip
*/
class MobiusStripApplication : public Application
{
public:
	MobiusStripApplication( int _count ) : count( _count ) {}
    MeshPtr _mobiusStrip;
    ShaderProgramPtr _markerShader;
    GLuint _sampler, _sampler2;
    int count;
    LightInfo _light;
    
    float _lr = 3.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    TexturePtr _Texture1, _Texture2;
    ShaderProgramPtr _shader;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        //Создаем MobiusStrip
        _mobiusStrip = makeMobiusStrip(count);
        _mobiusStrip->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        //Создаем шейдерную программу        
        _shader = std::make_shared<ShaderProgram>(data+"texture.vert", data+"texture.frag");
        _markerShader = std::make_shared<ShaderProgram>(data+"marker.vert", data+"marker.frag");

        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(1, 1, 1);
        _light.diffuse = glm::vec3(0, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);
        
        //=========================================================
        //Загрузка и создание текстур
        _Texture1 = loadTexture(data+"water.jpg");
        _Texture2 = loadTexture(data+"wood.png");
        
        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
        
        glGenSamplers(1, &_sampler2);
        glSamplerParameteri(_sampler2, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler2, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler2, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler2, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void update() override
    {
        Application::update();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Рисуем второй меш
        _shader->setMat4Uniform("modelMatrix", _mobiusStrip->modelMatrix());
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
        
        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);
        float time = glfwGetTime();
        float diff = (cos(time) + 1) / 2; // [0..1]
        _shader->setFloatUniform("time", time/3);
        _shader->setFloatUniform("diff", diff);
        
        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _Texture1->bind();
        glActiveTexture(GL_TEXTURE1);  //текстурный юнит 1
        glBindSampler(1, _sampler2);
        _Texture2->bind();
        _shader->setIntUniform("diffuseTex", 0);
        _shader->setIntUniform("diffuseTex2", 1);

        _shader->setMat4Uniform("modelMatrix", _mobiusStrip->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _mobiusStrip->modelMatrix()))));
            
        _mobiusStrip->draw();
    }

	void handleKey(int key, int scancode, int action, int mods) override
	{
		Application::handleKey(key, scancode, action, mods);

		const double multiplier = 0.5;

		if (action == GLFW_PRESS) {
			if (key == GLFW_KEY_MINUS) {
				count -= 2;
				_mobiusStrip = makeMobiusStrip(count);
				update();
			}
			if (key == GLFW_KEY_EQUAL) {
				count += 2;
				_mobiusStrip = makeMobiusStrip(count);
				update();
			}
		}
	}
};

int main()
{
    MobiusStripApplication app(200);
    app.start();

    return 0;
}